package dao;

import classes.TypeEmbarcation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TypeEmbarcationDAO extends DAO<TypeEmbarcation> {

    
    /** 
     * @return returns List<TypeEmbarcation>
     */
    @Override
    public List<TypeEmbarcation> recupAll() {
        // définition de la liste qui sera retournée en fin de méthode
        List<TypeEmbarcation> listeTypeEmbarcations = new ArrayList<TypeEmbarcation>();

        // déclaration de l'objet qui servira pour la requète SQL
        try {
            Statement requete = this.connect.createStatement();

            // définition de l'objet qui récupère le résultat de l'exécution de la requète
            ResultSet curseur = requete.executeQuery("select * from locaflot_facture.type_embarcation");

            // tant qu'il y a une ligne "résultat" à lire
            while (curseur.next()){
                // objet pour la récup d'une ligne de la table Club
                TypeEmbarcation unTypeEmbarcation = new TypeEmbarcation();

                unTypeEmbarcation.setCode(curseur.getString("codetype"));
                unTypeEmbarcation.setNom(curseur.getString("nomtype"));
                unTypeEmbarcation.setNbPlace(curseur.getInt("nbplace"));
                unTypeEmbarcation.setPrixDemiHeure(curseur.getDouble("prixdemiheure"));
                unTypeEmbarcation.setPrixHeure(curseur.getDouble("prixheure"));
                unTypeEmbarcation.setPrixDemiJour(curseur.getDouble("prixdemidemijour"));
                unTypeEmbarcation.setPrixJour(curseur.getDouble("prixjour"));


                listeTypeEmbarcations.add(unTypeEmbarcation);
            }

            curseur.close();
            requete.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listeTypeEmbarcations;
    }

    

    /**
     * @param obj instance of TypeEmbarcation that needs to be created 
     */
    @Override
    public void create(TypeEmbarcation obj) {
        try {
            PreparedStatement prepare = this.connect
                    .prepareStatement("INSERT INTO locaflot_facture.type_embarcation VALUES(?, ?, ?, ?, ?, ?, ?)"
                    ); 
            prepare.setString(1, obj.getCode());
            prepare.setString(2, obj.getNom());
            prepare.setDouble(3, obj.getNbPlace());
            prepare.setDouble(4, obj.getPrixDemiHeure());
            prepare.setDouble(5, obj.getPrixHeure());
            prepare.setDouble(6, obj.getPrixDemiJour());
            prepare.setDouble(7, obj.getPrixJour());


            prepare.executeUpdate();

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }


    
    /** 
     * @param code the code of the object in the table embarcation
     * @return returns an instance of TypeEmbarcation having the code provided
     */
    @Override
    public TypeEmbarcation read(int code) {

        TypeEmbarcation unTypeEmbarcation = new TypeEmbarcation();
        try {
            ResultSet result = this.connect
                    .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE)
                    .executeQuery("SELECT * FROM locaflot_facture.type_embarcation WHERE code = '" + code +"'");

            String codeRequete = String.valueOf(code);

            if(result.first())

                unTypeEmbarcation = new TypeEmbarcation(codeRequete, result.getString("nom"), result.getInt("nbplace"), result.getDouble("prix_demi_heure"), result.getDouble("prix_heure"), result.getDouble("prix_demi_jour"), result.getDouble("prix_jour"));
        }

        catch (SQLException e) {
            e.printStackTrace();
        }
        return unTypeEmbarcation;

    }


    
    /** 
     * @param obj instance of TypeEmbarcation that needs to be updated
     */
    @Override
    public void update(TypeEmbarcation obj) {
        try { this .connect
                .createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_UPDATABLE )
                .executeUpdate("UPDATE locaflot_facture.type_embarcation SET nom = '" + obj.getNom() + "'," +
                        "nb_place = '" + obj.getNbPlace() + "', prix_demi_heure = '" + obj.getPrixDemiHeure() + "' " +
                        ", prix_heure = '" + obj.getPrixHeure() + "', prix_demi_jour = '" + obj.getPrixDemiJour() + "' " +
                        ", prix_jour = '" + obj.getPrixJour() + "'WHERE code = '" + obj.getCode()+"'" );

            // obj = this.find(obj.getId()); // c'est quoi find ???

        }
        catch (SQLException e) {
            e.printStackTrace();
        }

    }


    
    /** 
     * @param obj instance of TypeEmbarcation that needs to be updated
     */
    @Override
    public void delete(TypeEmbarcation obj) {
        try {
            this.connect
                    .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE)
                    .executeUpdate("DELETE FROM locaflot_facture.type_embarcation WHERE code = '" + obj.getCode()+"'");

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}


