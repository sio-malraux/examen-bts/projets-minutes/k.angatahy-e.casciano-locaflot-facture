package dao;

import java.sql.*;

public class ConnexionPostgreSql {

    private static Connection connect;
    private static String url = "jdbc:postgresql://localhost:5432/projet_minute";
    private static String user = "k.angatahy";
    private static String passwd = "P@ssword";

    public static Connection getInstance(){
        if(connect == null){
            try {
                connect = DriverManager.getConnection(url, user, passwd);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connect;
    }
}
