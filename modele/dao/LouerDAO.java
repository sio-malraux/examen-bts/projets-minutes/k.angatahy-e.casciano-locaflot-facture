/*
package dao;

public class LouerDAO {

    ResultSet curseur = requete.executeQuery("select * from locaflot_facture.louer");

  uneLocation.setEmbarcation(curseur.getInt("id_embarcation"));
/////////////^ôui

    public void create(Embarcation obj) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO locaflot_facture.embarcation VALUES(?, ?, ?, ?)");
            prepare.setInt(1, obj.getId());
            // la suite à faire :)
        }
    }

 */
/*
package dao;

public class LouerDAO {

    ResultSet curseur = requete.executeQuery("select * from locaflot_facture.louer");

  uneLocation.setEmbarcation(curseur.getInt("id_embarcation"));
/////////////^ôui

    public void create(Embarcation obj) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO locaflot_facture.embarcation VALUES(?, ?, ?, ?)");
            prepare.setInt(1, obj.getId());
            // la suite à faire :)
        }
    }

 */

package dao;

import classes.ContratLocation;
import classes.Embarcation;
import classes.Louer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LouerDAO extends DAO<Louer> {

    
    /** 
     * @return returns the List<Louer>
     */
    @Override
    public List<Louer> recupAll() {
        // définition de la liste qui sera retournée en fin de méthode
        List<Louer> listeLocations = new ArrayList<Louer>();

        // déclaration de l'objet qui servira pour la requète SQL
        try {
            Statement requete = this.connect.createStatement();

            // définition de l'objet qui récupère le résultat de l'exécution de la requète
            ResultSet curseur = requete.executeQuery("select * from locaflot_facture.louer");

            // tant qu'il y a une ligne "résultat" à lire
            while (curseur.next()){
                // objet pour la récup d'une ligne de la table Club
                Louer uneLocation = new Louer();

                uneLocation.setNbPersonnes(curseur.getInt("nbpersonnes"));
                uneLocation.setUneEmbarcation(new Embarcation().find(curseur.getString("type")));
                uneLocation.setUnContratLocation(new ContratLocation().find(curseur.getInt("nbplace")));


                listeLocations.add(uneLocation);
            }

            curseur.close();
            requete.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listeLocations;
    }



    
    /** 
     * @param obj the instance of Louer that needs to be created 
     */
    // Insertion d'un objet Club dans la table Club (1 ligne)
    @Override
    public void create(Louer obj) {
        try {
            PreparedStatement prepare = this.connect
                    .prepareStatement("INSERT INTO locaflot_facture.Louer VALUES(?, ?, ?)"
                    ); // que faire ??
            prepare.setInt(1, obj.getNbPersonnes());
            prepare.setObject(2, obj.getUneEmbarcation());
            prepare.setObject(3, obj.getUnContratLocation());

            prepare.executeUpdate();

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }


    
    /** 
     * @param code the code of the object in the table Louer
     * @return returns an instance of Louer having the code provided
     */
    @Override
    public Louer read(int code) {

        Louer uneLocation = new Louer();
        try {
            ResultSet result = this.connect
                    .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE)
                    .executeQuery("SELECT * FROM locaflot_facture.Louer WHERE code = '" + code +"'");

            if(result.first())
                uneLocation = new Louer(result.getInt("nb_personnes"), new Embarcation(), new ContratLocation());
        }

        catch (SQLException e) {
            e.printStackTrace();
        }
        return uneLocation;

    }


    
    /** 
     * @param obj instance of Louer that needs to be updated
     */
    // Mise à jour de la disponibilité d'un TypeEmbarcation
    @Override
    public void update(Louer obj) {
        try { this .connect
                .createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_UPDATABLE )
                .executeUpdate("UPDATE locaflot_facture.louer SET nb_personnes = '" + obj.getNbPersonnes() + "'," +
                        "WHERE id_contrat = '" + obj.getUnContratLocation().getId()+"' AND id_embarcation = '"+ obj.getUneEmbarcation().getId() +"'" );

            // obj = this.find(obj.getId()); // c'est quoi find ???

        }
        catch (SQLException e) {
            e.printStackTrace();
        }

    }


    
    /** 
     * @param obj instance of Louer that needs to be deleted
     */
    // Suppression d'un Club
    @Override
    public void delete(Louer obj) {
        try {
            this.connect
                    .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE)
                    .executeUpdate("DELETE FROM locaflot_facture.type_embarcation WHERE id_contrat = '" + obj.getUnContratLocation().getId()+"' AND id_embarcation = '"+ obj.getUneEmbarcation().getId() +"'");

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}


