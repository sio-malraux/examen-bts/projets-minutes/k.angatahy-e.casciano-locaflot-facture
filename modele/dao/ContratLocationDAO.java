package dao;


import classes.ContratLocation;



import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.sql.Date;
import java.sql.Time;

public class ContratLocationDAO extends DAO<ContratLocation> {

    
    /** 
     * @return returns List<ContratLocation>
     */
    @Override
    public List<ContratLocation> recupAll() {
        // définition de la liste qui sera retournée en fin de méthode
        List<ContratLocation> listeContratsLocation = new ArrayList<ContratLocation>();

        // déclaration de l'objet qui servira pour la requète SQL
        try {
            Statement requete = this.connect.createStatement();

            // définition de l'objet qui récupère le résultat de l'exécution de la requète
            ResultSet curseur = requete.executeQuery("select * from locaflot_facture.contrat_location");

            // tant qu'il y a une ligne "résultat" à lire
            while (curseur.next()){
                // objet pour la récup d'une ligne de la table Club
                ContratLocation unContratLocation = new ContratLocation();

                //comment mettre un curseur.getLocalDate ou LocalTime

                unContratLocation.setId(curseur.getInt("id"));
                unContratLocation.setDate(curseur.getDate("date").toLocalDate());
                unContratLocation.setHeureDebut(curseur.getTime("heure_debut").toLocalTime());
                unContratLocation.setHeureFin(curseur.getTime("heure_fin").toLocalTime());


                listeContratsLocation.add(unContratLocation);
            }

            curseur.close();
            requete.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listeContratsLocation;
    }

    
    /**
     * @param obj instance of ContratLocation that needs to be created
     */
    @Override
    public void create(ContratLocation obj) {
        try {
            PreparedStatement prepare = this.connect
                    .prepareStatement("INSERT INTO locaflot_facture.contrat_location VALUES(?, ?, ?, ?)"
                    );
            prepare.setInt(1, obj.getId());
            prepare.setDate(2, java.sql.Date.valueOf(obj.getDate()));
            prepare.setTime(3, java.sql.Time.valueOf(obj.getHeureDebut()));
            prepare.setTime(3, java.sql.Time.valueOf(obj.getHeureFin()));


            prepare.executeUpdate();

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }


    
    /** 
     * @param id the id of the object in the table contrat_location
     * @return returns an instance of ContratLocation having the code provided 
     */
    @Override
    public ContratLocation read(int id) {

        ContratLocation unContratLocation = new ContratLocation();
        try {
            ResultSet result = this.connect
                    .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE)
                    .executeQuery("SELECT * FROM locaflot_facture.contrat_location WHERE id = '" + id +"'");

            if(result.first()) //TODO
                unContratLocation = new ContratLocation(id, result.getDate("couleur").toLocalDate(), result.getTime("heure").toLocalTime() ,result.getTime("heure_fin").toLocalTime());
        }

        catch (SQLException e) {
            e.printStackTrace();
        }
        return unContratLocation;

    }


    
    /** 
     * @param obj instance of ContratLocation that needs to be updated
     */
    @Override
    public void update(ContratLocation obj) {
        try { this .connect
                .createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_UPDATABLE ) //TODO
                .executeUpdate("UPDATE locaflot_facture.contrat_location SET date = '" + obj.getDate() + "', heure_debut = '" + obj.getHeureDebut() + "', heure_fin = '"
                        + obj.getHeureFin() + "' WHERE code = '" + obj.getId()+"'" );

            // obj = this.find(obj.getId()); // c'est quoi find ???

        }
        catch (SQLException e) {
            e.printStackTrace();
        }

    }


    
    /** 
     * @param obj instance of ContratLocation that needs to be deleted
     */
    // Suppression d'un Club
    @Override
    public void delete(ContratLocation obj) {
        try {
            this.connect
                    .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE)
                    .executeUpdate("DELETE FROM locaflot_facture.contrat_location WHERE id = '" + obj.getId()+"'");

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    
}

