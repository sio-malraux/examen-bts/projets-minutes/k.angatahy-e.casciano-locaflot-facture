package dao;

import classes.TypeEmbarcation;
import classes.Embarcation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class EmbarcationDAO extends DAO<Embarcation> {

    
    /** 
     * @return returns List<Embarcation>
     */
    @Override
    public List<Embarcation> recupAll() {
        // définition de la liste qui sera retournée en fin de méthode
        List<Embarcation> listeEmbarcations = new ArrayList<Embarcation>();

        // déclaration de l'objet qui servira pour la requète SQL
        try {
            Statement requete = this.connect.createStatement();

            // définition de l'objet qui récupère le résultat de l'exécution de la requète
            ResultSet curseur = requete.executeQuery("select * from locaflot_facture.embarcation");

            // tant qu'il y a une ligne "résultat" à lire
            while (curseur.next()){
                // objet pour la récup d'une ligne de la table Club
                Embarcation uneEmbarcation = new Embarcation();

                uneEmbarcation.setId(curseur.getInt("id"));
                uneEmbarcation.setCouleur(curseur.getString("couleur"));
                uneEmbarcation.setDisponible(curseur.getBoolean("disponible"));
                uneEmbarcation.setType(new TypeEmbarcation().find(curseur.getString("type")));
                listeEmbarcations.add(uneEmbarcation);
            }
            curseur.close();
            requete.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listeEmbarcations;
    }



    /**
     * @param obj instance of Embarcation that needs to be created 
     */
    @Override
    public void create(Embarcation obj) {
        try {
            PreparedStatement prepare = this.connect
                    .prepareStatement("INSERT INTO locaflot_facture.embarcation VALUES(?, ?, ?, ?)"
                    );
            prepare.setInt(1, obj.getId());
            prepare.setString(2, obj.getCouleur());
            prepare.setObject(3, obj.getType());
            prepare.setBoolean(4, obj.getDisponible());


            prepare.executeUpdate();

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }


    
    /** 
     * @param id the id of the object in the table Embarcation
     * @return returns an instance of Embarcation having the id provided
     */
    @Override
    public Embarcation read(int id) {

        Embarcation uneEmbarcation = new Embarcation();
        try {
            ResultSet result = this.connect
                    .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE)
                    .executeQuery("SELECT * FROM locaflot_facture.embarcation WHERE id = '" + id +"'");

            if(result.first())
                uneEmbarcation = new Embarcation(id, result.getString("couleur"),result.getBoolean("disponible"), new TypeEmbarcation().find(result.getString("type")));
        }

        catch (SQLException e) {
            e.printStackTrace();
        }
        return uneEmbarcation;

    }


    
    /** 
     * @param obj instance of Embarcation that needs to be upadated
     */
    // Mise à jour de la disponibilité d'une Embarcation
    @Override
    public void update(Embarcation obj) {
        try { this .connect
                .createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_UPDATABLE )
                .executeUpdate("UPDATE locaflot_facture.embarcation SET couleur = '" + obj.getCouleur() + "'," +
                        "disponible = '" + obj.getDisponible() + "', type = '" + obj.getType() + "' WHERE code = '" + obj.getId()+"'" );

           // obj = this.find(obj.getId()); // c'est quoi find ???

        }
        catch (SQLException e) {
            e.printStackTrace();
        }

    }


    
    /** 
     * @param obj insatnce of Embarcation that needs to be updated
     */
    // Suppression d'un Club
    @Override
    public void delete(Embarcation obj) {
        try {
            this.connect
                    .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_UPDATABLE)
                    .executeUpdate("DELETE FROM locaflot_facture.embarcation WHERE id = '" + obj.getId()+"'");

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
