package dao;

import java.sql.Connection;
import java.util.List;

public abstract class DAO<T> {

    /**
     * Attribut permettant d'établir la connexion à la BDD
     */
    public Connection connect = ConnexionPostgreSql.getInstance();

    /**
     * Méthode permettant de créer un objet de n'importe quelle classe
     * @param obj
     */
    public abstract void create(T obj);

    /**
     * Méthode permettant de lire 1 enregistrement dans une table
     * @param code
     * @return T
     */
    public abstract T read(int code);

    /**
     * Méthode permettant de mettre à jour un objet de n'importe quelle classe
     * @param obj
     */
    public abstract void update(T obj);

    /**
     * Méthode permettant de supprimer un objet de n'importe quelle classe
     * @param obj
     */
    public abstract void delete(T obj);

    /**
     * Méthode permettant de lire tous les enregistrements d'une table
     * @return List<T>
     */
    public abstract List<T> recupAll();

    // public abstract List<T> recupAllEmbByDispo(String type);


}