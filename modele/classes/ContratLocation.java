package classes;

import java.time.LocalDate;
import java.time.LocalTime;


public class ContratLocation {
    private int id;
    private LocalDate date;
    private LocalTime heureDebut;
    private LocalTime heureFin;
    private classes.ContratLocation ContratLocation;

    /**
     * Default constructor
     * Initialize properties with default values
     */
    public ContratLocation() {
        id = 0;
        date = LocalDate.now();
        heureDebut = LocalTime.now();
        heureFin = LocalTime.now();
    }

    /**
     * Constructor with parameters for initialization of properties
     * @param id : gives the identifier of the rental contract
     * @param date : gives the date of the rental contract
     * @param heureDebut : gives the rental start time
     * @param heureFin : gives the rental end time
     */
    public ContratLocation(int id, LocalDate date, LocalTime heureDebut, LocalTime heureFin) {
        this.id = id;
        this.date = date;
        this.heureDebut = heureDebut;
        this.heureFin = heureFin;
    }

    /** 
     * Gets the identifier of the rental contract
     * @return int
     */
    public int getId() {
        return id; 
    }

    /** 
     * Modifies the identifier of the rental contract
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }
        
    /** 
     * Gets the date of the rental contract
     * @return LocalDate
     */
    public LocalDate getDate() {
        return date; 
    }
    
    /** 
     * Modifies the date of the rental contract
     * @param date
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /** 
     * Gets the rental start time
     * @return LocalTime
     */
    public LocalTime getHeureDebut() {
        return heureDebut; 
    } 
    
    /** 
     * Modifies the rental start time
     * @param heureDebut
     */
    public void setHeureDebut(LocalTime heureDebut) {
        this.heureDebut = heureDebut;
    }
    
    /** 
     * Gets rental end time
     * @return LocalTime
     */
    public LocalTime getHeureFin() {
        return heureFin; 
    }

    /** 
     * Modifies the rental end time
     * @param heureFin
     */
    public void setHeureFin(LocalTime heureFin) {
        this.heureFin = heureFin;
    }

    public ContratLocation find(int nbplace) {
        return this.ContratLocation;
    }



}