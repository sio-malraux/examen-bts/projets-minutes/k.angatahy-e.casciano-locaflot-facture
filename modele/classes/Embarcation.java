package classes;

public class Embarcation {
    private int id;
    private String couleur;
    private boolean disponible;
    private String type;
    private classes.Embarcation Embarcation;

    /**
     * Default constructor
     * Initialize properties with default values
     */
    public Embarcation() {
        id = 0;
        couleur = "";
        disponible = true;
        type = "";
    }

    /**
     * Constructor with parameters for initialization of properties
     * @param id : gives the craft identifier
     * @param couleur : gives the craft color
     * @param disponible : gives the availability of the craft
     * @param type : gives the type of the craft
     */
    public Embarcation(int id, String couleur, boolean disponible, String type) {
        this.id = id;
        this.couleur = couleur;
        this.disponible = disponible;
        this.type = type;
    }
    
    /** 
     * Gets the identifier of the craft
     * @return int
     */
    public int getId() {
        return this.id;
    }

    /** 
     * Modifies the identifier of the craft
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /** 
     * Gets the color of the craft
     * @return String
     */
    public String getCouleur() {
        return this.couleur;
    }

    /** 
     * Modifies the color of the craft
     * @param couleur
     */
    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    /** 
     * Gets the availability of the craft
     * @return boolean
     */
    public boolean isDisponible() {
        return this.disponible;
    }

    /** 
     * Modifies the availability of the craft
     * @param disponible
     */
    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    /** 
     * Gets the type of the craft
     * @return String
     */
    public String getType() {
        return this.type;
    }

    /** 
     * Modifies the type of the craft
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    public Embarcation find(String type) {
        return this.Embarcation;
    }



}