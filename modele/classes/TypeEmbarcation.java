package classes;

public class TypeEmbarcation {
    private String code;
    private String nom;
    private int nbPlace;
    private double prixDemiHeure;
    private double prixHeure;
    private double prixDemiJour;
    private double prixJour;

    /**
     * Default constructor
     * Initialize properties with default values
     */
    public TypeEmbarcation() {
        code = "";
        nom = "";
        nbPlace = 0;
        prixDemiHeure = 0.0;
        prixHeure = 0.0;
        prixDemiJour = 0.0;
        prixJour = 0.0;
    }

    /**
     * Constructor with parameters for initialization of properties
     * @param code : gives the type code of the boat
     * @param nom : gives the name of the type of boat
     * @param nbPlace : gives the number of places of the type of the boat
     * @param prixDemiHeure : gives the price for the half hour rental of this type of boat
     * @param prixHeure : gives the price for the hour rental of this type of boat
     * @param prixDemiJour : gives the price for the half day rental of this type of boat
     * @param prixJour : gives the price for the day rental of this type of boat
     */
    public TypeEmbarcation(String code, String nom, int nbPlace, double prixDemiHeure, double prixHeure, double prixDemiJour, double prixJour) {
        this.code = code;
        this.nom = nom;
        this.nbPlace = nbPlace;
        this.prixDemiHeure = prixDemiHeure;
        this.prixHeure = prixHeure;
        this.prixDemiJour = prixDemiJour;
        this.prixJour = prixJour;
    }

    /** 
     * Gets the type code of the boat
     * @return String
     */
    public String getCode() {
        return this.code;
    }

    /** 
     * Modifies the type code of the boat
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /** 
     * Gets the name of the type of boat
     * @return String
     */
    public String getNom() {
        return this.nom;
    }

    /** 
     * Modifies the name of the type of boat
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /** 
     * Gets the number of places of the type of the boat
     * @return int
     */
    public int getNbPlace() {
        return this.nbPlace;
    }

    /** 
     * Modifies the number of places of the type of the boat
     * @param nbPlace
     */
    public void setNbPlace(int nbPlace) {
        this.nbPlace = nbPlace;
    }

    /** 
     * Gets the price for the half hour rental of this type of boat
     * @return double
     */
    public double getPrixDemiHeure() {
        return this.prixDemiHeure;
    }

    /** 
     * Modifies the price for the half hour rental of this type of boat
     * @param prixDemiHeure
     */
    public void setPrixDemiHeure(double prixDemiHeure) {
        this.prixDemiHeure = prixDemiHeure;
    }

    /** 
     * Gets the price for the hour rental of this type of boat
     * @return double
     */
    public double getPrixHeure() {
        return this.prixHeure;
    }

    /** 
     * Modifies the price for the hour rental of this type of boat
     * @param prixHeure
     */
    public void setPrixHeure(double prixHeure) {
        this.prixHeure = prixHeure;
    }

    /** 
     * Gets the price for the half day rental of this type of boat
     * @return double
     */
    public double getPrixDemiJour() {
        return this.prixDemiJour;
    }

    /** 
     * Modifies the price for the half day rental of this type of boat
     * @param prixDemiJour
     */
    public void setPrixDemiJour(double prixDemiJour) {
        this.prixDemiJour = prixDemiJour;
    }

    /** 
     * Gets the price for the day rental of this type of boat
     * @return double
     */
    public double getPrixJour() {
        return this.prixJour;
    }

    /** 
     * Modifies the price for the day rental of this type of boat
     * @param prixJour
     */
    public void setPrixJour(double prixJour) {
        this.prixJour = prixJour;
    }


}