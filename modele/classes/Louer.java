package classes;

public class Louer{
    private int nbPersonnes;
    private Embarcation uneEmbarcation;
    private ContratLocation unContratLocation;

    /**
     * Default constructor
     * Initialize properties with default values
     */
    public Louer() {
        nbPersonnes = 0;
        uneEmbarcation = null;
        unContratLocation = null;
    }

    /**
     * Constructor with parameters for initialization of properties
     * @param nbPersonnes : gives the number of persons for the rental of the craft
     * @param uneEmbarcation : gives the craft
     * @param unContratLocation : gives the rental contract
     */
    public Louer(int nbPersonnes, Embarcation uneEmbarcation, ContratLocation unContratLocation) {
        this.nbPersonnes = nbPersonnes;
        this.uneEmbarcation = uneEmbarcation;
        this.unContratLocation = unContratLocation;
    }

    /** 
     * Gets the number of persons for the rental of the boat
     * @return int
     */
    public int getNbPersonnes() {
        return this.nbPersonnes;
    }

    /** 
     * Modifies the number of persons for the rental of the boat
     * @param nbPersonnes
     */
    public void setNbPersonnes(int nbPersonnes) {
        this.nbPersonnes = nbPersonnes;
    }

    /** 
     * Gets the craft
     * @return Embarcation
     */
    public Embarcation getUneEmbarcation() {
        return this.uneEmbarcation;
    }

    /** 
     * Modifies the craft
     * @param uneEmbarcation
     */
    public void setUneEmbarcation(Embarcation uneEmbarcation) {
        this.uneEmbarcation = uneEmbarcation;
    }

    /** 
     * Gets the rental contract
     * @return ContratLocation
     */
    public ContratLocation getUnContratLocation() {
        return this.unContratLocation;
    }

    /** 
     * Modifies the rental contract
     * @param unContratLocation
     */
    public void setUnContratLocation(ContratLocation unContratLocation) {
        this.unContratLocation = unContratLocation;
    }


}